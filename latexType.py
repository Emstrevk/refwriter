import sys
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
from tkinter import *
import os.path


def Scan( title ):
    if sys.version_info > (3,0):
        choice = input(title)
    else:
        choice = raw_input(title)

    return choice

trail = False
page = 0

def Type( fileName, string ):

    #append = "/home/john/Dropbox/kandidatDSV/"

    global requestSpace
    output = string
    refName = os.path.basename(fileName)



    if "#" in string:
        output = "\n\\newline " + output + "\n"
    else:
        if (requestSpace == True):
            output = " " + output

    f = open(fileName, 'a')
    f.write( output )
    f.close()
    requestSpace = True

def Reference( fileName ):
    global requestSpace

    num = Scan("Reference page: ")
    name = Scan("Reference name: ")

    ref = "\cite[p. " + num + "]{<changeme> " + name + "}\n"

    f = open(fileName, 'a')
    f.write( ref )
    f.close()
    requestSpace = False

def NewLine( fileName ):
    global requestSpace

    output = "\\newline\n"

    f = open(fileName, 'a')
    f.write( output )
    f.close()
    print("Newline added")
    requestSpace = False


##Opening file fom tiny floating window and then killing it
root = Tk()
root.withdraw()
root.update()
if Scan("New file? (y/any): ") == "y":
    fileName = asksaveasfilename(initialdir = "~/Dropbox")
else:
    fileName = askopenfilename(initialdir = "~/Dropbox")
root.destroy()

#fileName = Scan("File name: ")

global requestSpace
requestSpace = False

while True:
    m_input = Scan("Input: ")

    if m_input[0] == ">":
        Reference(fileName)
    elif m_input[0] == "+":
        NewLine(fileName)
    elif m_input.upper() == "EXIT":
        break
    else:
        Type(fileName, m_input)
