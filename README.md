Keywords.py: Python application for typing references with minimal cognitive load. Keeps track of what page you're referencing with simple hotkeys.

Press "+" to increase page reference.
Type and enter "SETPAGE" to manually change page reference.

Type and enter your keywords or sentences and they will automatically be added as a bullet point at the end of the open file with a reference to the current page.

# Example use case:

1. Keywords.py open in terminal alongside academic journal
2. Press "+" each time you reach a new page while reading
3. Type as you read and enter to save reference sentences