import sys
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
from tkinter import *
import os.path


def Scan( title ):
    if sys.version_info > (3,0):
        choice = input(title)
    else:
        choice = raw_input(title)

    return choice

trail = False
page = 0

def Type( fileName, string, onPage):

    #append = "/home/john/Dropbox/kandidatDSV/"

    output = string
    refName = os.path.basename(fileName)

    if "#" not in string:
        output += " [" + refName + ", p." + str(onPage) + "]"

    if string != "":
        if string[0] == ">":
            output = "\n    - " + output[1:]
        else:
            output = "\n\n" + output
    else: output = string


    f = open(fileName, 'a')
    f.write( output )
    f.close()


##Opening file fom tiny floating window and then killing it
root = Tk()
root.withdraw()
root.update()
if Scan("New file? (y/any): ") == "y":
    fileName = asksaveasfilename(initialdir = "~/Dropbox")
else:
    fileName = askopenfilename(initialdir = "~/Dropbox")
root.destroy()

#fileName = Scan("File name: ")

while True:
    m_input = Scan("Input: ")

    if m_input.upper() == "SETPAGE":
        #Ensure numerical
        while True:
            num = Scan("Page: ")
            try:
                page = int(num)
                print("Page: " + str(page))
                break
            except ValueError:
                continue
    elif m_input.upper() == "EXIT":
        break
    elif m_input == "+":
        page += 1
        print("Page: " + str(page))
    else:
        Type(fileName, m_input, page)
