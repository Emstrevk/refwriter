import sys
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
from tkinter import *
import os.path


def Scan( title ):
    if sys.version_info > (3,0):
        choice = input(title)
    else:
        choice = raw_input(title)

    return choice

def Type( file, author, onPage, string):

    #append = "/home/john/Dropbox/kandidatDSV/"

    output = string

    if "#" not in string:
        output += " [" + author + ", p." + onPage + "]"

    if string[0] == ">":
        output = "\n    - " + output[1:]
    else:
        output = "\n\n" + output


    f = open(file, 'a')
    f.write( output )
    f.close()

path = "/home/john/Dropbox/kandidatDSV/"
fileNameProblem = path + "ProblemRefs"
fileNameReq = path + "ReqRefs"
fileNameSimilar = path + "SimilarRefs"
fileNameMisc = path + "MiscRefs"

currentFilePath = fileNameMisc

warning = '\x1b[0;32;40m'
endWarning = '\033[0m'

while True:
    #read input
    m_input = Scan(endWarning + "Summary: ")
    #Ask for category
    while True:
        num = Scan("Category: 1. Problem, 2. Requirements, 3. Similar Solutions, 4. Misc")
        try:
            select = int(num)
            if (select == 1):
                currentFilePath = fileNameProblem
            elif (select == 2):
                currentFilePath = fileNameReq
            elif (select == 3):
                currentFilePath = fileNameSimilar
            else:
                currentFilePath = fileNameMisc
            break
        except ValueError:
            continue
    #Ask for number
    pages = Scan(warning + "Page(s): ")
    #Ask for author
    auth = Scan(warning + "Author: ")
    Type(currentFilePath, auth, pages, m_input)
